-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        10.5.9-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 cxuuweb.cxuu_admin_group 结构
CREATE TABLE IF NOT EXISTS `cxuu_admin_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `systemrole` text DEFAULT NULL COMMENT '系统权限序列',
  `channlrole` text DEFAULT NULL COMMENT '内容栏目权限序列',
  `menurole` text DEFAULT NULL COMMENT '系统菜单权限序列',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='管理员角色组表';

-- 正在导出表  cxuuweb.cxuu_admin_group 的数据：5 rows
/*!40000 ALTER TABLE `cxuu_admin_group` DISABLE KEYS */;
INSERT INTO `cxuu_admin_group` (`id`, `groupname`, `systemrole`, `channlrole`, `menurole`) VALUES
	(1, '超级管理员', 'a:5:{i:0;s:11:"index_index";i:4;s:11:"cache_index";i:18;s:12:"notice_index";i:42;s:16:"admingroup_index";i:53;s:15:"adminuser_index";}', 'a:8:{i:0;s:1:"5";i:1;s:1:"8";i:2;s:1:"6";i:3;s:1:"1";i:4;s:1:"2";i:5;s:1:"7";i:6;s:1:"4";i:7;s:1:"3";}', NULL),
	(2, '内容管理员', 'a:13:{i:0;s:11:"index_index";i:1;s:10:"index_home";i:2;s:12:"system_index";i:12;s:15:"adminmenu_index";i:23;s:17:"contentcate_index";i:27;s:16:"content_cate_all";i:28;s:13:"content_index";i:29;s:11:"content_add";i:30;s:12:"content_edit";i:43;s:15:"adminuser_index";i:44;s:13:"adminuser_add";i:47;s:22:"adminuser_switchStatus";i:48;s:22:"adminuser_passwordedit";}', 'a:9:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"9";i:3;s:1:"2";i:4;s:1:"7";i:5;s:1:"5";i:6;s:2:"10";i:7;s:1:"6";i:8;s:1:"8";}', 'a:15:{i:0;s:2:"12";i:1;s:2:"13";i:2;s:2:"14";i:3;s:2:"15";i:4;s:2:"11";i:5;s:1:"5";i:6;s:1:"6";i:7;s:2:"16";i:8;s:1:"7";i:9;s:2:"17";i:10;s:1:"4";i:11;s:1:"1";i:12;s:2:"18";i:13;s:2:"22";i:14;s:2:"25";}'),
	(3, '事业部', 'a:16:{i:0;s:11:"index_index";i:1;s:10:"index_home";i:4;s:11:"cache_index";i:6;s:18:"cache_delFileCache";i:16;s:12:"notice_index";i:23;s:17:"contentcate_index";i:28;s:13:"content_index";i:29;s:11:"content_add";i:30;s:12:"content_edit";i:31;s:17:"content_attribute";i:32;s:14:"content_status";i:33;s:20:"content_switchstatus";i:34;s:11:"content_del";i:35;s:18:"index_articlecount";i:36;s:16:"admingroup_index";i:43;s:15:"adminuser_index";}', 'a:6:{i:0;s:1:"5";i:1;s:1:"8";i:2;s:1:"6";i:3;s:1:"1";i:4;s:1:"2";i:5;s:1:"3";}', 'a:8:{i:0;s:2:"14";i:1;s:2:"15";i:2;s:2:"11";i:3;s:1:"5";i:4;s:1:"6";i:5;s:2:"16";i:6;s:1:"7";i:7;s:2:"17";}'),
	(4, '市场部', 'N;', 'a:2:{i:0;s:1:"5";i:1;s:1:"6";}', 'a:3:{i:0;s:2:"14";i:1;s:2:"15";i:2;s:2:"11";}'),
	(5, '123', NULL, 'a:3:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"7";}', NULL);
/*!40000 ALTER TABLE `cxuu_admin_group` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_admin_menu 结构
CREATE TABLE IF NOT EXISTS `cxuu_admin_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '上级ID',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `controller` varchar(50) DEFAULT NULL COMMENT '所属控制器',
  `action` varchar(50) DEFAULT NULL COMMENT '当前操作方法',
  `ico` varchar(100) DEFAULT NULL COMMENT '显示图标',
  `sort` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='系统后台菜单表';

-- 正在导出表  cxuuweb.cxuu_admin_menu 的数据：23 rows
/*!40000 ALTER TABLE `cxuu_admin_menu` DISABLE KEYS */;
INSERT INTO `cxuu_admin_menu` (`id`, `pid`, `name`, `controller`, `action`, `ico`, `sort`) VALUES
	(1, 4, '缓存管理', 'cache', 'index', 'layui-icon-theme', 2),
	(2, 4, '系统设置', 'system', 'index', 'layui-icon-set', 3),
	(3, 4, '系统公告', 'notice', 'index', 'layui-icon-notice', 1),
	(4, 0, '系统管理', 'menu', '#', 'layui-icon-app', 4),
	(5, 0, '管理员管理', 'menu', '#', 'layui-icon-username', 3),
	(6, 5, '管理员列表', 'adminuser', 'index', 'layui-icon-vercode', 1),
	(7, 5, '角色列表', 'admingroup', 'index', 'layui-icon-home', 3),
	(9, 4, '系统菜单', 'adminmenu', 'index', 'layui-icon-templeate-1', 5),
	(11, 14, '添加栏目', 'contentcate', 'add&view', 'layui-icon-senior', 2),
	(12, 0, '文章内容', 'menu', 'content', 'layui-icon-tabs', 1),
	(13, 12, '全部内容', 'content', 'index', 'layui-icon-password', 1),
	(14, 0, '栏目管理', 'menu', '#', 'layui-icon-align-left', 2),
	(15, 14, '栏目列表', 'contentcate', 'index', 'layui-icon-spread-left', 1),
	(16, 5, '添加管理员', 'adminuser', 'add&view', 'layui-icon-add-1', 2),
	(17, 5, '添加角色', 'admingroup', 'add&view', 'layui-icon-user', 4),
	(18, 0, '扩展功能', 'menu', '#', 'layui-icon-util', 6),
	(19, 18, '值班安排', 'onduty', 'index', 'layui-icon-tabs', 1),
	(20, 4, '网站访问量', 'visit', 'index', 'layui-icon-date', 6),
	(21, 18, '图集功能', 'image', 'index', 'layui-icon-picture', 3),
	(22, 18, '领导信息', 'member', 'index', 'layui-icon-user', 4),
	(23, 4, '操作日志', 'log', 'index', 'layui-icon-list', 4),
	(24, 4, '附件列表', 'attment', 'index', 'layui-icon-release', 7),
	(25, 18, '意见反馈', 'feedback', 'index', 'layui-icon-survey', 4);
/*!40000 ALTER TABLE `cxuu_admin_menu` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_admin_user 结构
CREATE TABLE IF NOT EXISTS `cxuu_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) DEFAULT NULL COMMENT '角色ID',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `logintime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `loginip` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- 正在导出表  cxuuweb.cxuu_admin_user 的数据：5 rows
/*!40000 ALTER TABLE `cxuu_admin_user` DISABLE KEYS */;
INSERT INTO `cxuu_admin_user` (`id`, `gid`, `username`, `password`, `nickname`, `logintime`, `loginip`, `status`) VALUES
	(1, 1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '邓中华', '2021-04-11 12:13:03', '127.0.0.1', 1),
	(2, 2, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test', '2021-04-11 14:47:13', '127.0.0.1', 1),
	(3, 3, 'test2', '0659c7992e268962384eb17fafe88364', '测试', '2021-04-11 14:29:00', '127.0.0.1', 1),
	(4, 2, 'test1', '2a8451cdc0d0e5f2e96cb17640e60d3d', '李四', NULL, NULL, 1),
	(5, 2, 'admin22', 'c81e728d9d4c2f636f067f89cc14862c', '左左工', NULL, NULL, 1);
/*!40000 ALTER TABLE `cxuu_admin_user` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_article 结构
CREATE TABLE IF NOT EXISTS `cxuu_article` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cid` int(10) NOT NULL DEFAULT 0 COMMENT '栏目ID',
  `title` varchar(200) DEFAULT NULL COMMENT '内容标题',
  `attribute_a` tinyint(1) DEFAULT NULL COMMENT '文章属性 头条',
  `attribute_b` tinyint(1) DEFAULT NULL COMMENT '文章属性 小头条',
  `attribute_c` tinyint(1) DEFAULT NULL COMMENT '文章属性 轮换',
  `attid` varchar(200) DEFAULT NULL COMMENT '附件地址ID',
  `examine` varchar(10) DEFAULT NULL COMMENT '审核人',
  `img` varchar(100) DEFAULT NULL COMMENT '缩略图地址',
  `imgbl` tinyint(1) NOT NULL DEFAULT 0 COMMENT '判断是否有图片',
  `time` datetime DEFAULT NULL COMMENT '添加时间',
  `hits` int(5) DEFAULT 1 COMMENT '浏览次数',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `uid` int(5) DEFAULT NULL COMMENT '用户ID',
  `gid` int(5) DEFAULT NULL COMMENT '用户组ID',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='文章内容基本信息表';

-- 正在导出表  cxuuweb.cxuu_article 的数据：51 rows
/*!40000 ALTER TABLE `cxuu_article` DISABLE KEYS */;
INSERT INTO `cxuu_article` (`id`, `cid`, `title`, `attribute_a`, `attribute_b`, `attribute_c`, `attid`, `examine`, `img`, `imgbl`, `time`, `hits`, `status`, `uid`, `gid`) VALUES
	(1, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(2, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 2, 0, NULL, 0),
	(3, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 6, 1, NULL, 0),
	(4, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 2, 1, NULL, 0),
	(5, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(6, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2019-10-01 19:11:19', 2, 1, NULL, 0),
	(7, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 4, 1, NULL, 0),
	(8, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(9, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(10, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, NULL, 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(11, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(12, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 2, 1, NULL, 0),
	(13, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', '', 0, '2021-04-11 19:11:19', 9, 1, NULL, 0),
	(14, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(15, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 85, 1, NULL, 0),
	(16, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 62, 1, NULL, 0),
	(17, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 10, 1, NULL, 0),
	(18, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 23, 1, NULL, 0),
	(19, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 4, 1, NULL, 0),
	(20, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(21, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(22, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(39, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 1, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 7, 1, NULL, 0),
	(38, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(28, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 24, 1, NULL, 0),
	(29, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(30, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(31, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(32, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 1, 1, NULL, 0),
	(33, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 5, 1, NULL, 0),
	(34, 8, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 3, 1, NULL, 0),
	(35, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 13, 1, NULL, 0),
	(37, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '', 0, '2021-04-11 19:11:19', 7, 0, NULL, 0),
	(40, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 0, '0', NULL, '/uploads/img/202010/01/demo.jpg', 0, '2021-04-11 19:11:19', 5, 1, 1, 0),
	(41, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, '0,4', '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 5, 0, 2, 0),
	(42, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '新华社', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 13, 1, 1, NULL),
	(43, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 1, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 6, 1, 1, 1),
	(44, 6, '龙啸轩内容管理系统V3演示文章', 1, 1, 1, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 99, 1, 1, 1),
	(45, 6, '龙啸轩内容管理系统V3演示文章', 1, NULL, 1, '1', '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 95, 1, 1, 1),
	(46, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, 1, '0', '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 40, 1, 1, 1),
	(47, 6, '龙啸轩内容管理系统V3演示文章', 1, NULL, NULL, '1', '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 270, 1, 1, 1),
	(48, 9, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', '', 0, '2021-04-11 19:11:19', 12, 1, 1, 1),
	(49, 3, '龙啸轩内容管理系统V3演示文章', 1, NULL, 1, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 56, 1, 1, 1),
	(50, 6, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 82, 1, 1, 1),
	(51, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, '1', '李四', '', 0, '2021-04-11 19:11:19', 18, 1, 1, 1),
	(52, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, '2,30', '经工', '/uploads/img/202010/01/demo.jpg', 1, '2021-04-11 19:11:19', 42, 1, 1, 1),
	(53, 7, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '在在', '', 0, '2021-04-11 19:11:19', 25, 1, 1, 1),
	(54, 10, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', '', 0, '2021-04-11 19:11:19', 28, 1, 1, 1),
	(55, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '新华社', '', 0, '2021-04-11 19:11:19', 2, 1, 1, 1),
	(56, 3, '龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '新华社', '', 0, '2021-04-11 19:11:19', 7, 1, 1, 1),
	(57, 3, 'abc龙啸轩内容管理系统V3演示文章', NULL, NULL, NULL, NULL, '经工', NULL, 0, '2021-04-11 14:29:28', 1, 1, 3, 3);
/*!40000 ALTER TABLE `cxuu_article` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_article_cate 结构
CREATE TABLE IF NOT EXISTS `cxuu_article_cate` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) DEFAULT NULL COMMENT '栏目名称',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '栏目类型-频道或列表',
  `theme` varchar(50) DEFAULT NULL COMMENT '模板路径',
  `sort` int(10) DEFAULT NULL COMMENT '栏目排序',
  `num` int(5) DEFAULT NULL COMMENT '前台列表显示内容条数',
  `color` varchar(50) DEFAULT '0' COMMENT '栏目颜色',
  `ct` tinyint(1) DEFAULT 1 COMMENT '统计开关',
  `urlname` varchar(50) DEFAULT NULL COMMENT '自定义访问路径名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='文章内容栏目表';

-- 正在导出表  cxuuweb.cxuu_article_cate 的数据：9 rows
/*!40000 ALTER TABLE `cxuu_article_cate` DISABLE KEYS */;
INSERT INTO `cxuu_article_cate` (`id`, `pid`, `name`, `type`, `theme`, `sort`, `num`, `color`, `ct`, `urlname`) VALUES
	(1, 0, '新闻咨询', 0, NULL, 1, NULL, '#ffd700', 0, NULL),
	(2, 1, '国际新闻', 0, NULL, 2, 25, '#ff7800', 0, NULL),
	(3, 1, '公司要闻', 1, NULL, 1, 20, '#b13f3f', 0, 'gsyw'),
	(5, 0, '产品列表', 0, NULL, 2, NULL, '#1e9fff', 0, NULL),
	(6, 5, '电子产品', 1, NULL, 1, 10, '#009688', 1, 'dzcp2'),
	(7, 2, '资料管理', 1, NULL, 3, NULL, '#8549ba', 0, NULL),
	(8, 5, '生活用品', 1, NULL, 1, NULL, '#c71585', 1, NULL),
	(9, 1, '文章内容', 1, NULL, 1, 25, '#acc236', 1, NULL),
	(10, 5, '建筑产品', 1, NULL, NULL, NULL, '#90ee90', 1, NULL);
/*!40000 ALTER TABLE `cxuu_article_cate` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_article_content 结构
CREATE TABLE IF NOT EXISTS `cxuu_article_content` (
  `aid` int(11) NOT NULL COMMENT '文章ID',
  `content` mediumtext DEFAULT NULL COMMENT '详细内容',
  PRIMARY KEY (`aid`),
  KEY `aid` (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章内容主体表';

-- 正在导出表  cxuuweb.cxuu_article_content 的数据：7 rows
/*!40000 ALTER TABLE `cxuu_article_content` DISABLE KEYS */;
INSERT INTO `cxuu_article_content` (`aid`, `content`) VALUES
	(51, '<p style="text-align: center;"><img src="/uploads/img/202010/01/demo.jpg" alt="" width="600" height="434" /></p>\n<p style="text-align: center;">龙啸轩内容管理系统V3演示文章</p>'),
	(42, '<p style="text-align: center;"><img src="/uploads/img/202010/01/demo.jpg" alt="" width="600" height="434" /></p>\n<p style="text-align: center;">龙啸轩内容管理系统V3演示文章</p>'),
	(44, '<p style="text-align: center;"><img src="/uploads/img/202010/01/demo.jpg" alt="" width="600" height="434" /></p>\n<p style="text-align: center;">龙啸轩内容管理系统V3演示文章</p>'),
	(54, '<p style="text-align: center;"><img src="/uploads/img/202010/01/demo.jpg" alt="" width="600" height="434" /></p>\n<p style="text-align: center;">龙啸轩内容管理系统V3演示文章</p>'),
	(49, '<p style="text-align: center;"><img src="/uploads/img/202010/01/demo.jpg" alt="" width="600" height="434" /></p>\n<p style="text-align: center;">龙啸轩内容管理系统V3演示文章</p>'),
	(48, '<p>1</p>'),
	(57, '<p>1111</p>');
/*!40000 ALTER TABLE `cxuu_article_content` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_attments 结构
CREATE TABLE IF NOT EXISTS `cxuu_attments` (
  `attid` int(11) NOT NULL AUTO_INCREMENT,
  `atturl` varchar(200) DEFAULT NULL COMMENT '附件地址',
  `priname` varchar(150) DEFAULT NULL COMMENT '原始名',
  `ext` varchar(5) DEFAULT NULL COMMENT '文件类型',
  `size` int(10) DEFAULT NULL COMMENT '文件大小',
  `userid` int(11) DEFAULT NULL COMMENT '上传用户ID',
  PRIMARY KEY (`attid`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='系统附件表';

-- 正在导出表  cxuuweb.cxuu_attments 的数据：9 rows
/*!40000 ALTER TABLE `cxuu_attments` DISABLE KEYS */;
INSERT INTO `cxuu_attments` (`attid`, `atturl`, `priname`, `ext`, `size`, `userid`) VALUES
	(1, '/uploads/img/202010/01/demo.jpg', '1.txt', '.txt', 98, 2),
	(2, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 2),
	(4, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 2),
	(6, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 2),
	(9, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 2),
	(27, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 1),
	(28, '/uploads/img/202010/01/demo.jpg', '新建文本文档.txt', '.txt', 74, 1),
	(31, '/uploads/img/202010/01/demo.jpg', '新建文本文档 (2).txt', '.txt', 260, 1),
	(32, '/uploads/img/202010/01/demo.jpg', '新建文本文档 (2).txt', '.txt', 260, 1);
/*!40000 ALTER TABLE `cxuu_attments` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_feedbacks 结构
CREATE TABLE IF NOT EXISTS `cxuu_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '意见标题',
  `name` varchar(50) DEFAULT NULL COMMENT '提交人昵称或姓名',
  `phone` varchar(12) DEFAULT NULL COMMENT '提交人联系电话',
  `content` text DEFAULT NULL COMMENT '意见内容',
  `ip` varchar(12) DEFAULT NULL COMMENT '用户IP',
  `mid` int(11) DEFAULT NULL COMMENT '对应提交意见的成员ID',
  `time` datetime DEFAULT NULL COMMENT '提交意见时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '显示状态',
  `re_content` text DEFAULT NULL COMMENT '回复内容',
  `re_status` tinyint(1) DEFAULT NULL COMMENT '回复显示状态',
  `re_time` datetime DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='意见提交表';

-- 正在导出表  cxuuweb.cxuu_feedbacks 的数据：0 rows
/*!40000 ALTER TABLE `cxuu_feedbacks` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxuu_feedbacks` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_images 结构
CREATE TABLE IF NOT EXISTS `cxuu_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `img` varchar(150) DEFAULT NULL COMMENT '缩略图',
  `auther` varchar(50) DEFAULT NULL COMMENT '作者',
  `content` text DEFAULT NULL COMMENT '描述内容',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='图集基本信息表';

-- 正在导出表  cxuuweb.cxuu_images 的数据：2 rows
/*!40000 ALTER TABLE `cxuu_images` DISABLE KEYS */;
INSERT INTO `cxuu_images` (`id`, `title`, `img`, `auther`, `content`, `status`, `time`) VALUES
	(1, '拉萨扫黑除恶宣传片二', '/uploads/img/202010/01/demo.jpg', '汪汪', '拉萨扫黑除恶宣传片二', 1, '2021-04-11 19:11:19'),
	(2, '宇宙管理局召开治理银河系专题工作会议', '/uploads/img/202010/01/demo.jpg', '汪汪', '2143214', 1, '2021-04-11 19:11:19');
/*!40000 ALTER TABLE `cxuu_images` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_images_image 结构
CREATE TABLE IF NOT EXISTS `cxuu_images_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL COMMENT '分类ID',
  `imgsrc` varchar(150) DEFAULT NULL COMMENT '图片地址',
  `info` text DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='图集详细内容表';

-- 正在导出表  cxuuweb.cxuu_images_image 的数据：6 rows
/*!40000 ALTER TABLE `cxuu_images_image` DISABLE KEYS */;
INSERT INTO `cxuu_images_image` (`id`, `aid`, `imgsrc`, `info`) VALUES
	(1, 1, '/uploads/img/202010/01/demo.jpg', '415'),
	(2, 1, '/uploads/img/202010/01/demo.jpg', '2'),
	(3, 1, '/uploads/img/202010/01/demo.jpg', '142'),
	(4, 1, '/uploads/img/202010/01/demo.jpg', '4235'),
	(5, 2, '/uploads/img/202010/01/demo.jpg', '1234242'),
	(6, 2, '', '');
/*!40000 ALTER TABLE `cxuu_images_image` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_log_sql 结构
CREATE TABLE IF NOT EXISTS `cxuu_log_sql` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `ca` varchar(50) DEFAULT NULL COMMENT '操作控制器和方法',
  `loginip` varchar(15) DEFAULT NULL COMMENT '登录IP',
  `useragent` varchar(500) DEFAULT NULL COMMENT '客户端信息',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `dothing` varchar(200) DEFAULT NULL COMMENT '操作描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='数据库操作日志表';

-- 正在导出表  cxuuweb.cxuu_log_sql 的数据：0 rows
/*!40000 ALTER TABLE `cxuu_log_sql` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxuu_log_sql` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_member 结构
CREATE TABLE IF NOT EXISTS `cxuu_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `duties` varchar(50) DEFAULT NULL COMMENT '管理工作内容',
  `photo` varchar(150) DEFAULT NULL COMMENT '照片',
  `duty` varchar(150) DEFAULT NULL COMMENT '职务',
  `sort` int(3) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='公司或单位成员表';

-- 正在导出表  cxuuweb.cxuu_member 的数据：2 rows
/*!40000 ALTER TABLE `cxuu_member` DISABLE KEYS */;
INSERT INTO `cxuu_member` (`id`, `name`, `duties`, `photo`, `duty`, `sort`, `status`) VALUES
	(1, '周凯', '总经理', '/uploads/img/202010/01/demo.jpg', '主管XX工作', 1, 1),
	(2, '常迎霞', '市场经理', '/uploads/img/202010/01/demo.jpg', '分管XX工作', 2, 1);
/*!40000 ALTER TABLE `cxuu_member` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_notices 结构
CREATE TABLE IF NOT EXISTS `cxuu_notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `img` varchar(100) DEFAULT NULL COMMENT '附图',
  `content` mediumtext DEFAULT NULL COMMENT '内容',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `status` tinyint(1) DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='系统公告表';

-- 正在导出表  cxuuweb.cxuu_notices 的数据：3 rows
/*!40000 ALTER TABLE `cxuu_notices` DISABLE KEYS */;
INSERT INTO `cxuu_notices` (`id`, `title`, `img`, `content`, `time`, `status`) VALUES
	(2, '龙啸轩内容管理系统V3', NULL, '22', '2021-04-11 19:11:19', 1),
	(3, '龙啸轩内容管理系统V3', '', 'asfr', '2021-04-11 19:11:19', 1),
	(4, 'cxuuweb   3.0来了', NULL, '这个系统是全新构建的，效率高，扩展性强！', '2021-04-11 19:11:19', 0);
/*!40000 ALTER TABLE `cxuu_notices` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_onduty 结构
CREATE TABLE IF NOT EXISTS `cxuu_onduty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `juname` varchar(50) DEFAULT NULL COMMENT '局值班人',
  `chuname` varchar(50) DEFAULT NULL COMMENT '处值班人',
  `yuanname` varchar(50) DEFAULT NULL COMMENT '值班员',
  `phone` varchar(12) DEFAULT NULL COMMENT '职务',
  `ondutytime` date DEFAULT NULL COMMENT '值班时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='值班安排表';

-- 正在导出表  cxuuweb.cxuu_onduty 的数据：2 rows
/*!40000 ALTER TABLE `cxuu_onduty` DISABLE KEYS */;
INSERT INTO `cxuu_onduty` (`id`, `juname`, `chuname`, `yuanname`, `phone`, `ondutytime`, `status`) VALUES
	(1, '张某', '李某', '周某', '68888884', '2021-04-11', 1),
	(2, '张某', '李某', '周某', '13889009372', '2021-04-11', 0);
/*!40000 ALTER TABLE `cxuu_onduty` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_siteconfig 结构
CREATE TABLE IF NOT EXISTS `cxuu_siteconfig` (
  `name` varchar(50) NOT NULL COMMENT '主键',
  `data` text DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- 正在导出表  cxuuweb.cxuu_siteconfig 的数据：3 rows
/*!40000 ALTER TABLE `cxuu_siteconfig` DISABLE KEYS */;
INSERT INTO `cxuu_siteconfig` (`name`, `data`) VALUES
	('siteinfo', 'a:5:{s:8:"sitename";s:27:"龙啸轩内容管理系统";s:7:"siteurl";s:20:"http://www.cxuu.top/";s:8:"keywords";s:15:"龙啸轩网络";s:8:"descript";s:15:"龙啸轩网络";s:9:"copyright";s:88:"龙啸轩内容管理系统 便捷易用的网站内容管理平台  浏览器支持IE8+";}'),
	('upload', 'a:10:{s:13:"imguploadsize";s:4:"2048";s:13:"attuploadsize";s:4:"1024";s:12:"imguploadext";s:20:".jpg|.png|.jpeg|.gif";s:12:"attuploadext";s:40:".doc|.docx|.zip|.7z|.zip|.xls|.xlsx|.txt";s:7:"thumbON";s:1:"1";s:8:"maxWidth";s:3:"600";s:9:"maxHeight";s:3:"600";s:7:"waterON";s:1:"1";s:8:"waterImg";N;s:8:"waterPos";s:1:"0";}'),
	('cache', 'a:6:{s:9:"indexhtml";s:1:"1";s:13:"indexhtmltime";s:1:"5";s:11:"visitscache";N;s:9:"visitsnum";s:1:"3";s:10:"visitstime";s:1:"8";s:14:"visitsshowtime";s:2:"10";}');
/*!40000 ALTER TABLE `cxuu_siteconfig` ENABLE KEYS */;

-- 导出  表 cxuuweb.cxuu_visits 结构
CREATE TABLE IF NOT EXISTS `cxuu_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL COMMENT '日期',
  `visits` int(11) DEFAULT NULL COMMENT '计数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COMMENT='网站前台访问量表';

-- 正在导出表  cxuuweb.cxuu_visits 的数据：60 rows
/*!40000 ALTER TABLE `cxuu_visits` DISABLE KEYS */;
INSERT INTO `cxuu_visits` (`id`, `date`, `visits`) VALUES
	(39, '2020-04-29', 111),
	(38, '2020-04-28', 101),
	(37, '2020-04-26', 61),
	(36, '2020-04-25', 31),
	(35, '2020-04-24', 31),
	(34, '2020-04-22', 432),
	(33, '2020-04-23', 326),
	(40, '2020-05-02', 1),
	(41, '2020-05-03', 31),
	(42, '2020-05-04', 22),
	(43, '2020-05-05', 156),
	(44, '2020-05-06', 102),
	(45, '2020-05-10', 51),
	(46, '2020-05-13', 153),
	(47, '2020-05-14', 396),
	(48, '2020-05-15', 93),
	(49, '2020-05-16', 33),
	(50, '2020-05-17', 24),
	(51, '2020-06-20', 12),
	(52, '2020-06-28', 33),
	(53, '2020-06-29', 90),
	(54, '2020-06-30', 24),
	(55, '2020-07-21', 37),
	(56, '2020-07-29', 30),
	(57, '2020-08-08', 6),
	(58, '2020-08-19', 234),
	(59, '2020-08-20', 15),
	(60, '2020-08-27', 9),
	(61, '2020-09-05', 78),
	(62, '2020-09-13', 25),
	(68, '2020-09-21', 6),
	(67, '2020-09-20', 196),
	(69, '2020-09-22', 546),
	(70, '2020-09-24', 48),
	(71, '2020-09-25', 4),
	(72, '2020-09-26', 18),
	(73, '2020-09-28', 2),
	(74, '2020-09-29', 12),
	(75, '2020-09-30', 13),
	(76, '2020-10-07', 75),
	(77, '2020-10-09', 118),
	(78, '2020-10-11', 139),
	(79, '2020-10-15', 150),
	(80, '2020-10-17', 6),
	(81, '2020-10-26', 13),
	(82, '2020-10-29', 407),
	(83, '2020-10-31', 22),
	(84, '2020-11-01', 6),
	(85, '2020-11-02', 14),
	(86, '2020-11-03', 55),
	(87, '2020-11-04', 101),
	(88, '2020-11-07', 54),
	(89, '2020-11-23', 3),
	(90, '2020-12-02', 32),
	(91, '2020-12-03', 246),
	(92, '2021-02-12', 4),
	(93, '2021-03-05', 30),
	(94, '2021-03-08', 2),
	(95, '2021-03-09', 2),
	(96, '2021-04-11', 98);
/*!40000 ALTER TABLE `cxuu_visits` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
