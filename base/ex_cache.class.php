<?php

/***
 * 公共自定义缓存应用基类
 */

namespace root\base;

use z\cache;

class ex_cache
{
    public static function setCache($ckey, $data, $timeout)
    {
        switch ($GLOBALS['ZPHP_CONFIG']['cxuu_cache_mod'] ?? 0) {
            case 1:
                return cache::R($ckey, $data, $timeout, 2);
            case 2:
                return cache::M($ckey, $data, $timeout, 2);
            default:
                return cache::F($ckey, $data, $timeout, 2);
        }
    }

    public static function getCache($ckey)
    {
        switch ($GLOBALS['ZPHP_CONFIG']['cxuu_cache_mod'] ?? 0) {
            case 1:
                return cache::R($ckey);
            case 2:
                return cache::M($ckey);
            default:
                return cache::F($ckey);
        }
    }
}
