<?php

/**
 * 设置数据缓存方法
 * $ckey 缓存键名，如：test_0001
 * $data 要缓存的数据，如：array('a','b'),可以是变量、字符、数组及所有数据类型
 * $timeout 缓存的生命周期 秒，如：660
 * * 使用方法：在模板<?php echo setCache('test_0001',array('a','b'),600);?> 
 */

function setCache($ckey, $data, $timeout)
{
    if (empty(getCache($ckey))) {
        \root\base\ex_cache::setCache($ckey, $data, $timeout);
    }
}

//过滤变量   HTML 转义字符 '"<>& 以及 ASCII 值小于 32 的字符。
function varFilter($var)
{
    return trim(filter_var($var, FILTER_SANITIZE_SPECIAL_CHARS))?:null;
    /* if(preg_match("/\'/",$x))
    return json(array('status' => 0, 'info' => '包含非法字符')); */
}


/**
 * 获取数据缓存方法
 * $ckey 缓存对应的键名，如：test_0001
 *  * 使用方法：在模板<?php echo getCache('test_0001');?> 
 * @return [type] 数据类型和缓存的数据对应 
 */
function getCache($ckey)
{
    return \root\base\ex_cache::getCache($ckey);
}


/**
 * 输出json格式数据（脚本中断）
 * @param  [type] $data [description]
 * @return [type]       [description]
 */
function json($data = null)
{
    header('Content-Type:application/json; charset=utf-8');
    die(json_encode($data, JSON_UNESCAPED_UNICODE));
}


/**
 * 页面跳转
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function redirect($url = null)
{
    header("Content-type: text/html; charset=utf-8");
    die(header("Location:" . $url));
}


/**
 * 获取客户端ip地址
 * @return string
 */
function getip()
{
    $ip = false;
    empty($_SERVER['HTTP_CLIENT_IP']) || $ip = $_SERVER['HTTP_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ips = explode(', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip) {
            array_unshift($ips, $ip);
            $ip = false;
        }
        $count = count($ips);
        for ($i = 0; $i != $count; $i++) {
            if (!preg_match('/^(10│172.16│192.168)./', $ips[$i])) {
                $ip = $ips[$i];
                break;
            }
        }
    }
    return $ip ?: $_SERVER['REMOTE_ADDR'];
}

/**
 * 格式化显示时间
 * $t DateTime
 * $f Y/m/d H:i:s
 * 使用方法：在模板<?php echo fTime('2020-05-05','Y-m-d');?> 
 * */
function fTime($t, $f)
{
    $date = date_create($t);
    $ftime =  date_format($date, $f);
    return $ftime;
}

/**
 * 时间转化为多久前
 * @param  $date datetime 时间
 * @return date('Y-m-d H:i:s',time());
 */

function hTime($date)
{
    $time = strtotime($date);
    $time = time() - $time;
    if (is_numeric($time)) {
        $value = array(
            "years" => 0, "days" => 0, "hours" => 0,
            "minutes" => 0, "seconds" => 0,
        );
        if ($time >= 31556926) {
            $value["years"] = floor($time / 31556926);
            $time = ($time % 31556926);
            $t = $value["years"] . '年前';
        } elseif (31556926 > $time && $time >= 86400) {
            $value["days"] = floor($time / 86400);
            $time = ($time % 86400);
            $t = $value["days"] . '天前';
        } elseif (86400 > $time && $time >= 3600) {
            $value["hours"] = floor($time / 3600);
            $time = ($time % 3600);
            $t = $value["hours"] . '小时前';
        } elseif (3600 > $time && $time >= 60) {
            $value["minutes"] = floor($time / 60);
            $time = ($time % 60);
            $t = $value["minutes"] . '分钟前';
        } else {
            $t = $time . '秒前';
        }
        return $t;
    } else {
        return date('Y-m-d H:i:s', time());
    }
}



/*
 * 根据二维数组某个字段的值查找数组
 * @param array $data 待过滤数组
 * @param string $field 要查找的字段
 * @param $value 要查找的字段值
 * @return array 返回所有符合要求的数组集合
	$data = array( 
		0=>array( 
		        'id'=>1, 
		        'name'=>'a' 
		), 
		.... 
		3=>array( 
		        'id'=>4, 
		        'name'=>'d' 
		), 
	); 
	$res = filter_by_value($data,'name','d'); 
	$result['name'] = array_column($res,'name');
	$result['theme'] = array_column($res,'theme');
	最终输出：
	array(2) {
	  ["name"] =>array(1) {
		[0] =>string(12) "电子产品"
	  }
	  ["theme"] =>array(1) {
		[0] =>string(0) ""
	  }
	} */

function filter_by_value(array $data, string $field, $value)
{
    $data = array_filter($data, function ($row) use ($field, $value) {
        if (isset($row[$field])) {
            return $row[$field] == $value;
        }
    });
    return $data;
}