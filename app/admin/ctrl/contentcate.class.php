<?php

namespace ctrl;

use model\article_cate;
use z\view;

class contentcate
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$m = new article_cate;
			$list = $m->SelectData(1);
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = '';
			if (!isset($_GET['mod'])) { //根据传递值判断生成输出数据模式，用于LAYUI treeTable或dtree调用(内容菜单或栏目管理调用)
				$jsonlist['data'] = generateTree($list);
			} else {
				$jsonlist['data'] = contentCateTree($list);
			}
			exit(json_encode($jsonlist));
		}
		view::display();
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			$m = new article_cate;
			$list = $m->SelectTreeData();
			view::assign('list', $list);
			view::display();
		} else {
			$m = new article_cate;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			$m = new article_cate;
			$info = $m->FindData();
			$list = $m->SelectTreeData();
			view::assign('list', $list);
			view::assign('info', $info);
			view::display();
		} else {
			$m = new article_cate;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}


	public static function del()
	{
		if (isset($_POST['id'])) {
			$find =  [
				'id' => $_POST['id']
			];
			$m = new article_cate;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			} else {
				json(array('status' => 0, 'info' => '该栏目下有内容，拒绝删除！'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
