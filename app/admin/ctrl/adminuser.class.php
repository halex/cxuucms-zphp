<?php

namespace ctrl;

use model\admin_user;
use model\admin_group;
use z\view;

class adminuser
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new admin_user;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}


	public static function add()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$groupList = $m->selectGroup();
			view::assign('groupList', $groupList);
			view::display();
		} else {
			$m = new admin_user;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}


	public static function edit()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$groupList = $m->selectGroup();
			$u = new admin_user;
			$info = $u->findData();
			view::assign('groupList', $groupList);
			view::assign('info', $info);
			view::display();
		} else {
			$m = new admin_user;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function switchStatus()
	{
		$m = new admin_user;
		$edit = $m->switchStatus();
		if ($edit) {
			json(array('status' => 1, 'info' => '设置成功'));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new admin_user;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}


	public static function passwordedit()
	{
		if (isset($_GET['view'])) {
			$u = new admin_user;
			$info = $u->findData();
			view::assign('info', $info);
			view::display();
		} else {
			$m = new admin_user;
			$result = $m->passwordEdit();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}
}
