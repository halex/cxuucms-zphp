<?php

namespace ctrl;

use model\admin_menu;
use z\view;

class adminmenu
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new admin_menu;
			$result = $d->selectDataAuth();
			$jsonlist = array();
			$jsonlist['code'] = 1;
			$jsonlist['data'] = generateTree($result['all']);
			exit(json_encode($jsonlist));
		} else if (isset($_GET['tree_json'])) {
			$d = new admin_menu;
			$result = $d->selectData();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['data'] = menuCateTree($result['all']);
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			$d = new admin_menu;
			$list = $d->selectData();
			view::assign('list', $list['top']);
			view::display();
		} else {
			$m = new admin_menu;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			if (isset($_GET['id'])) {
				$d = new admin_menu;
				$list = $d->selectData();
				$info = $d->findData();
				view::assign('list', $list['top']);
				view::assign('info', $info);
				view::display();
			} else {
				return '缺少参数:id';
			}
		} else {
			$m = new admin_menu;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}


	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new admin_menu;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
