<?php

namespace ctrl;

use model\admin_menu;
use z\view;

class menu
{
    static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		//\common\middleware\check::auth(); //判断用户权限
    }
    
    public static function index()
    {
        view::display();
    }

    public static function content()
    {
        view::display();
    }


    public static function system()
    {
        $d = new admin_menu;
        $list = $d->selectDataAuth();
        view::assign('list', $list);
        view::display();
    }

}
