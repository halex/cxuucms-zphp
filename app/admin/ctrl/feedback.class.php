<?php

namespace ctrl;

use model\feedbacks;
use z\view;

class feedback
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new feedbacks;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			view::display();
		} else {
			if (empty($_POST['title'])) {
				json(array('status' => 0, 'info' => '请填写必填数据！'));
			}
			$m = new feedbacks;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			if (isset($_GET['id'])) {
				$m = new feedbacks;
				$feedback = $m->findData();
				view::assign('feedback', $feedback);
				view::display();
			} else {
				return '缺少参数:id';
			}
		} else {
			if (empty($_POST['title'])) {
				json(array('status' => 0, 'info' => '请填写必填数据！'));
			}
			$m = new feedbacks;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function switchStatus()
	{
		if (isset($_POST['id'])) {
			$m = new feedbacks;
			$edit = $m->switchStatus();
			if ($edit) {
				json(array('status' => 1, 'info' => '设置成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new feedbacks;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
