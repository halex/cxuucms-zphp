<?php

namespace ctrl;

use model\notices;
use model\article;
use z\view;

class index
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		view::display();
	}


	public static function home()
	{
		$m = new notices;
		$noticeList = $m->selectData(3);
		view::assign('noticeList', $noticeList);
		view::display();
	}


}
