<?php

namespace ctrl;

use model\attments;
use model\siteconfig;
use common\extend\Image;

class upload
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	static function imgUpload()
	{

		$d = new siteconfig;
		$upConfig = $d->getConfig('upload');
		$ext = explode('|', $upConfig['imguploadext']);
		$size = $upConfig['imguploadsize'];

		$up = new \ext\upload();
		$date = date('Ym', time());
		$time = date('d', time());
		$up->set('path', P_PUBLIC . 'uploads/img/' . $date . '/' . $time);        //定义文件上传路径
		$up->set('allowType', $ext);   //定义允许上传的文件后缀['.jpg', '.gif', '.png', '.jpeg']
		$up->set('maxSize', $size * 1024);               //定义允许上传的最大尺寸
		$up->upload();                      //执行上传
		$info = $up->getInfo();                       //返回上传文件信息，索引数组
		$err = $up->getError();                       //返回错误信息，数组

		//缩略图
		if ($upConfig['thumbON'] == 1) {
			Image::thumb($info[0]['path'], $info[0]['path'], '', $upConfig['maxWidth'], $upConfig['maxHeight']);
		}

		//加水印
		if ($upConfig['waterON'] == 1) {
			Image::water($info[0]['path'], P_PUBLIC . 'res/img/waterimg/logo.png', $upConfig['waterPos']);
		}

		if ($info) {
			json(array('result' => $info, 'status' => 1, 'info' => '上传成功'));
		} else {
			json(array('status' => 0, 'info' => $err));
		}
	}

	static function attmentUpload()
	{
		$d = new siteconfig;
		$upConfig = $d->getConfig('upload');
		$ext = explode('|', $upConfig['attuploadext']);
		$size = $upConfig['attuploadsize'];

		$up = new \ext\upload();
		$date = date('Ym', time());
		$time = date('d', time());
		$up->set('path', P_PUBLIC . 'uploads/attment/' . $date . '/' . $time);        //定义文件上传路径
		$up->set('allowType', $ext);   //定义允许上传的文件后缀['.doc', '.xls', '.xlsx', '.docx', '.rar', '.7z', '.txt','.zip']
		$up->set('maxSize', $size * 1024);               //定义允许上传的最大尺寸
		$up->upload();                      //执行上传
		$info = $up->getInfo();                       //返回上传文件信息，索引数组
		$err = $up->getError();                       //返回错误信息，数组

		$m = new attments;					//写入数据库，返回主键
		$prekey = $m->insertData($info[0]['src'], $info[0]['originName'], $info[0]['suffix'], $info[0]['size']);

		if ($info) {
			//写入日志
			$log_sql = new \model\log_sql();
			$log_sql->insertData("上传附件：" . $info[0]['src']);

			json(array('result' => $info, 'status' => 1, 'info' => '上传成功', 'prekey' => $prekey));
		} else {
			json(array('status' => 0, 'info' => $err));
		}
	}


	static function tinymceUpload()
	{
		$d = new siteconfig;
		$upConfig = $d->getConfig('upload');
		$ext = explode('|', $upConfig['imguploadext']);
		$size = $upConfig['imguploadsize'];
		$up = new \ext\upload();
		$date = date('Ym', time());
		$time = date('d', time());
		$up->set('path', P_PUBLIC . 'uploads/img/' . $date . '/' . $time);        //定义文件上传路径
		$up->set('allowType', $ext);   //定义允许上传的文件后缀['.jpg', '.gif', '.png', '.jpeg']
		$up->set('maxSize', $size * 1024);             //定义允许上传的最大尺寸
		$up->upload();                      //执行上传
		$info = $up->getInfo();                       //返回上传文件信息，索引数组		//"result":[{"name":"5e4f4e423b3610.jpg","suffix":"jpg","originName":"92532836.jpg","type":"image/jpeg","size":272594,"path":"D:/PHP/htdocs/Z-PHP/public_html/public/uploads/img/5e4f4e423b3610.jpg","src":"/public/uploads/img/5e4f4e423b3610.jpg"}]
		$err = $up->getError();                       //返回错误信息，数组
		//缩略图
		if ($upConfig['thumbON'] == 1) {
			Image::thumb($info[0]['path'], $info[0]['path'], '', $upConfig['maxWidth'], $upConfig['maxHeight']);
		}
		//加水印
		if ($upConfig['waterON'] == 1) {
			Image::water($info[0]['path'], P_PUBLIC . 'res/img/waterimg/logo.png', $upConfig['waterPos']);
		}

		if ($info) {
			json(array('location' => $info[0]['src'], 'status' => 200));
		} else {
			json(array('res' => $err, 'status' => 0));
		}
	}
}
