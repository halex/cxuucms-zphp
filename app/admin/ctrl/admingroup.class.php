<?php

namespace ctrl;

use model\admin_group;
use z\view;

class admingroup
{
	static function init()
	{
		\common\middleware\check::isLogin(); //判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new admin_group;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			view::display();
		} else {
			if (empty($_POST['groupname'])) {
				json(array('status' => 0, 'info' => '请填写必填数据！'));
			}
			$m = new admin_group;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$info = $m->findData();
			view::assign('info', $info);
			view::display();
		} else {
			if (empty($_POST['groupname'])) {
				json(array('status' => 0, 'info' => '请填写必填数据！'));
			}
			$m = new admin_group;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}


	public static function del()
	{
		if ($_POST['id'] == 1) {
			json(array('status' => 0, 'info' => '不能删除超级管理员组！'));
		}
		$m = new admin_group;
		$del = $m->deleteOneData();
		if ($del) {
			json(array('status' => 1, 'info' => '删除成功'));
		} else {
			json(array('status' => 0, 'info' => '该用户组下有用户，请先删除组下管理员用户！'));
		}
	}

	//系统权限设置
	public static function systemrole()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$info = $m->findData();
			view::assign('systemrole', empty($info['systemrole']) ?'': unserialize($info['systemrole']));
			view::assign('info', $info);
			view::display();
		} else {
			$m = new admin_group;
			$result = $m->updateSystemroleData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	//系统栏目设置
	public static function channlrole()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$info = $m->findData();
			if (!empty($info['channlrole']))
				view::assign('channlid', implode(",", unserialize($info['channlrole'])));
			else
				view::assign('channlid', '');
			view::assign('info', $info);
			view::display();
		} else {
			$m = new admin_group;
			$result = $m->updateChannlroleData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	//系统菜单设置
	public static function menurole()
	{
		if (isset($_GET['view'])) {
			$m = new admin_group;
			$info = $m->findData();
			if (!empty($info['menurole']))
				view::assign('menuid', implode(",", unserialize($info['menurole'])));
			else
				view::assign('menuid', '');
			view::assign('info', $info);
			view::display();
		} else {
			$m = new admin_group;
			$result = $m->updateMenuroleData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}
}
