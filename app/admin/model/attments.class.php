<?php

namespace model;

use root\base\model;

class attments extends model
{
	//关联查找一条数据
	public function findData()
	{
		$db = $this->db();
		$where['id'] = $_GET['id'];
		return $db->table('attments')->where($where)->find();
	}

	//添加数据
	public function insertData($atturl, $priname, $ext, $size)
	{
		$db = $this->db('attments');
		$data = [
			'atturl' => $atturl,
			'userid' => sessionInfo("userid"),
			'priname' => $priname,
			'ext' => $ext,
			'size' => $size,
		];
		return $db->insert($data);
	}


	//删除一条数据
	public function deleteOneData()
	{
		$db = $this->db();
		$where['attid'] = $_GET['attid'];
		$find = $db->table('attments')->where($where)->find();
		if ($find) {
			del_file($find['atturl']); //删除文件
			insertSqlLog("删除附件 attID：" . $find['attid'] . " 路径：" . $find['atturl']);//写入日志
			return $db->table('attments')->where($where)->Delete();
		}else{
			return false;
		}
	}

	//获取数据并分页
	public function jsonPageSelect()
	{
		$db = $this->db();
		$p = intval($_GET['page'] ?? 0) ?: 1;
		$num = intval($_GET['limit'] ?? 0) ?: 15;

		$page = ['num' => $num, 'p' => $p, 'return' => true];
		$result['data'] = $db->table('attments')->Page($page)->order('attid DESC')->select();
		$result['page'] = $db->getPage();
		return $result;
	}
}
