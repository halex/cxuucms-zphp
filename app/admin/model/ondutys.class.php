<?php

namespace model;

use root\base\model;

class ondutys extends model
{
    //关联查找一条数据
    public function findData()
    {
        $db = $this->db();
        $where['id'] = $_GET['id'];
        return $db->table('onduty')->where($where)->find();
    }
    //查询列表
    public function selectData($limit)
    {
        $db = $this->db();
        $where['status'] = 1;
        return $db->table('onduty')->where($where)->limit($limit)->cache(600)->Order('id DESC')->select();
    }

    //添加数据
    public function insertData()
    {
        if (empty($_POST['juname']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db('onduty');
        $data = [
            'juname' => varFilter($_POST['juname']),
            'chuname' => varFilter($_POST['chuname']),
            'yuanname' => varFilter($_POST['yuanname']),
            'phone' => varFilter($_POST['phone']),
            'ondutytime' => varFilter($_POST['ondutytime']),
            'status' => $_POST['status'],
        ];
        $re_key = $db->insert($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("添加值班 ID：" . $re_key);
            //更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "onduty");
            return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
        } else {
            return ['status' => 0, 'msg' => '数据错误！添加失败'];
        }
    }
    //更新数据
    public function updateData()
    {
        if (empty($_POST['juname']))
			return  ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $data = [
            'juname' => varFilter($_POST['juname']),
            'chuname' => varFilter($_POST['chuname']),
            'yuanname' => varFilter($_POST['yuanname']),
            'phone' => varFilter($_POST['phone']),
            'ondutytime' => varFilter($_POST['ondutytime']),
            'status' => $_POST['status'],
        ];
        $re_key = $db->table('onduty')->Where($where)->Update($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("修改值班 ID：" . $_POST['id']);
            //更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "onduty");

            return ['status' => 1, 'msg' => '修改成功'];
        } else {
            return ['status' => 0, 'msg' => '数据没有变化'];
        }
    }

    //删除一条数据
    public function deleteOneData()
    {
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $result = $db->table('onduty')->where($where)->Delete();
        //写入日志
        insertSqlLog("删除值班 ID：" . $_POST['id']);

        return $result;
    }

    //设置状态
    public function switchStatus()
    {
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $data = [
            'status' => $_POST['status']
        ];
        $result = $db->table('onduty')->where($where)->Update($data);
        //写入日志
        insertSqlLog("切换值班状态 ID：" . $_POST['id']);

        return $result;
    }

    //获取数据并分页
    public function jsonPageSelect()
    {
        $db = $this->db();
        $p = intval($_GET['page'] ?? 0) ?: 1;
        $num = intval($_GET['limit'] ?? 0) ?: 15;

        $page = ['num' => $num, 'p' => $p, 'return' => true];
        $result['data'] = $db->table('onduty')->Page($page)->order('id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
}
