<?php

namespace model;

use root\base\model;

class admin_menu extends model
{
	//查找一条数据
	public function findData()
	{
		$db = $this->db();
		$where['id'] = $_GET['id'];
		return $db->table('admin_menu')->where($where)->find();
	}

	 //根据权限查找菜单数据
	 public function selectData()
	 {
	 	$db = $this->db();
		$result['all'] =  $db->table('admin_menu')->Order('sort ASC')->cache(0)->Select();
		$objet = new \common\extend\Category(array('id', 'pid', 'name'));
		$result['top'] = $objet->getPid($result['all'], 0);
		return $result;
	 }

	 //系统路径显示，前端根据数据进行判断
	public static function getUrlData()
	{
		$db = \ext\db::Init();
		$data = $db->table('admin_menu')->cache(0)->Select();
		$ctrl = filter_by_value($data, 'controller', ROUTE['ctrl']);
		$pid = array_column($ctrl, 'pid');
		$topctrl = filter_by_value($data, 'id', $pid[0]);
		$topname = array_column($topctrl, 'name');
		$result['topname'] = $topname[0];
		return $result;
	}


	//根据权限查找菜单数据
	public function selectDataAuth()
	{
		$db = $this->db();
		$where = '';
		if (sessionInfo('gid') != 1) {
			$where = "id IN (" . adminMenuRole() . ")";
		}
		$result = [];
		$result['all'] = $db->table('admin_menu')->where($where)->Order('sort ASC')->cache(0)->Select();
		$objet = new \common\extend\Category(array('id', 'pid', 'name'));
		$result['pid'] = $objet->getPid($result['all'], 0);
		return $result;
	}

	//添加数据
	public function insertData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		//插入数据信息部分
		$db = $this->db('admin_menu');
		$data = [
			'pid' => $_POST['pid'],
			'name' => varFilter($_POST['name']),
			'controller' => varFilter($_POST['controller']),
			'action' => varFilter($_POST['action']),
			'sort' => varFilter($_POST['sort']),
			'ico' => varFilter($_POST['ico']),
		];
		$re_key = $db->insert($data); //$this->error($db->getError());
		if ($re_key) {
			//写入日志
			insertSqlLog("添加系统菜单：" . $_POST['name']);

			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "admin_menu");

			return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
		} else {
			return ['status' => 0, 'msg' => '数据错误！添加失败'];
		}
	}
	//更新数据
	public function updateData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'pid' => $_POST['pid'],
			'name' => varFilter($_POST['name']),
			'controller' => varFilter($_POST['controller']),
			'sort' => varFilter($_POST['sort']),
			'action' => varFilter($_POST['action']),
			'ico' => varFilter($_POST['ico']),
		];
		$re_key = $db->table('admin_menu')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改系统菜单：" . $_POST['name']);

			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "admin_menu");

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//删除一条数据
	public function deleteOneData()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		//写入日志
		insertSqlLog("删除系统菜单 ID" . $_POST['id']);
		return $db->table('admin_menu')->where($where)->Delete();
	}
}
