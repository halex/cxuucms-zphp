<?php

namespace model;

use root\base\model;

class images_image extends model
{
    //查找一条数据
    public function findData()
    {
        $db = $this->db();
        $where['id'] = $_GET['id'];
        return $db->table('images_image')->where($where)->find();
    }

    //查找一条所属图集的数据
    public function findImagesData()
    {
        $db = $this->db();
        $where['id'] = $_GET['aid'];
        return $db->table('images')->where($where)->find();
    }

    //添加数据
    public function insertData()
    {
        if (empty($_POST['imgsrc']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db('images_image');
        $data = [
            'aid' => $_POST['aid'],
            'imgsrc' => varFilter($_POST['imgsrc']),
            'info' => varFilter($_POST['info']),
        ];
        $re_key = $db->insert($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("添加图集图片信息ID：" . $re_key . "图集ID：" . $_POST['aid']);

            return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
        } else {
            return ['status' => 0, 'msg' => '数据错误！添加失败'];
        }
    }
    //更新数据
    public function updateData()
    {
        if (empty($_POST['imgsrc']))
        return ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $data = [
            'imgsrc' => varFilter($_POST['imgsrc']),
            'info' => varFilter($_POST['info']),
        ];
        $re_key = $db->table('images_image')->Where($where)->Update($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("修改图集图片信息ID：" . $_POST['id']);

            return ['status' => 1, 'msg' => '修改成功'];
        } else {
            return ['status' => 0, 'msg' => '数据没有变化'];
        }
    }

    //删除一条数据
    public function deleteOneData()
    {
        $db = $this->db();
        $where['id'] = $_POST['id'];
        //写入日志
        insertSqlLog("删除图集图片信息ID：" . $_POST['id']);

        return $db->table('images_image')->where($where)->Delete();
    }



    //获取数据并分页
    public function jsonPageSelect()
    {
        $db = $this->db();
        $p = intval($_GET['page'] ?? 0) ?: 1;
        $num = intval($_GET['limit'] ?? 0) ?: 15;
        $where['aid'] = $_GET['aid'];

        $page = ['num' => $num, 'p' => $p, 'return' => true];
        $result['data'] = $db->table('images_image')->where($where)->Page($page)->order('id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
}
