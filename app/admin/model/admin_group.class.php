<?php

namespace model;

use root\base\model;

class admin_group extends model
{

	//查找一条数据
	public function findData($find = null)
	{
		$db = parent::db();
		$where['id'] = $find ?? $_GET['id'];
		return $db->table('admin_group')->where($where)->find();
	}

	//查询列表
	public function selectGroup()
	{
		$db = parent::db();
		return $db->table('admin_group')->select();
	}

	//获取数据并分页
	public function jsonPageSelect()
	{
		$db = $this->db();
		$p = intval($_GET['page'] ?? 0) ?: 1;
		$num = intval($_GET['limit'] ?? 0) ?: 10;

		$page = ['num' => $num, 'p' => $p, 'return' => true];
		$result['data'] = $db->table('admin_group')->Page($page)->order('id DESC')->select();
		$result['page'] = $db->getPage();
		return $result;
	}

	//判断用户组名是否存在
	private function checkNameData()
	{
		$db = $this->db();
		if (empty($_POST['id'])) {
			$where['groupname'] = $_POST['groupname'];
		} else {
			//验证用户组名是否存在 查询不等当前ID的数据进行比对
			$where = [
				'id <>' => $_GET['id'],
				'groupname' => $_POST['groupname'],
			];
		}
		return $db->table('admin_group')->where($where)->find();
	}
	//添加数据
	public function insertData()
	{
		//验证用户组名是否存在
		if ($this->checkNameData())
			return ['status' => 0, 'msg' => '用户组名已经存在！'];
		if (empty($_POST['groupname']))
			return ['status' => 0, 'msg' => '请填写必要数据'];

		$db = $this->db();
		$data = [
			'groupname' => varFilter($_POST['groupname']),
		];
		$re_key = $db->table('admin_group')->insert($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("添加角色：" . $_POST['groupname']);
			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "admin_group");

			return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
		} else {
			return ['status' => 0, 'msg' => '数据错误！添加失败'];
		}
	}

	//更新数据
	public function updateData()
	{
		//验证用户组名是否存在
		if ($this->checkNameData())
			return ['status' => 0, 'msg' => '用户组名已经存在！'];
		if (empty($_POST['groupname']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'groupname' => varFilter($_POST['groupname']),
		];
		$re_key = $db->table('admin_group')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改角色：" . $_POST['groupname']);
			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "admin_group");
			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//更新数据
	public function updateSystemroleData()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'systemrole' => serialize($_POST['systemrole']),
		];
		$re_key = $db->table('admin_group')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改角色系统操作权限 ID：", $_POST['id']);

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//更新数据
	public function updateChannlroleData()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'channlrole' => serialize($_POST['channlrole']),
		];
		$re_key = $db->table('admin_group')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改角色栏目权限 ID：" . $_POST['id']);

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//更新数据
	public function updateMenuroleData()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'menurole' => serialize($_POST['menurole']),
		];
		$re_key = $db->table('admin_group')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改角色系统菜单权限 ID：" . $_POST['id']);

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//删除一条数据
	public function deleteOneData()
	{
		$db = $this->db();
		$find['gid'] = $_POST['id'];
		$find = $db->table('admin_user')->where($find)->find();
		if ($find) {
			return false; //如该角色下有管理员，拒绝删除角色
		} else {
			$where['id'] = $_POST['id'];			
			insertSqlLog("删除角色 ID：" . $_POST['id']);//写入日志
			return $db->table('admin_group')->where($where)->Delete();
		}
	}
}
