<?php
namespace model;

class article
{
	//搜索列表
    public static function searchJoinData()
    {
        $db = \ext\db::Init();
		$p = $_GET['p'];
        $num = 20;  //每页显示条数
        $page = ['num' => $num, 'p' => $p, 'return' => ['prev', 'next', 'first', 'last', 'list']];
        $field = 'b.id as cateid , b.name as catename ,a.id,a.cid,a.title,a.status,a.time';
        $where['a.title LIKE'] = '%'.$_GET['keywords'].'%' ;
		$join = 'LEFT JOIN article_cate b ON a.cid=b.id';	
        $result['data'] = $db->table('article a')->field($field)->Join($join)->page($page)->where($where)->order('a.id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
}
