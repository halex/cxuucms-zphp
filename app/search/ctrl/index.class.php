<?php
namespace ctrl;

use model\article;
use z\view;

class index
{
    public static function index()
    {
		if(!isset($_GET['keywords']) || empty($_GET['keywords'])){
			echo('<div style="text-align:center;margin:auto;width:1140px;margin-top:50px;"><h2>请输入搜索关键字！</h2></div>');
			exit;
		}
		$d = new article;
		$list = $d->searchJoinData();
		view::assign('list',$list['data']);
		view::assign('page',$list['page']);
		view::display();
    }
}
