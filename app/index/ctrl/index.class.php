<?php

namespace ctrl;

use model\siteconfig;
use z\view;

class index
{
	// static function init()
	// {
	// 	\model\visits::insertData(); //用户访问记录 写入记数
	// }

	// public static function index()
	// {
	// 	if (siteconfig::getCache('indexhtml') == 1) {
	// 		$html = view::GetCache(siteconfig::getCache('indexhtmltime'), 'index.html');
	// 		if ($html) {
	// 			echo $html;
	// 		} else {
	// 			view::Display();
	// 		}
	// 	} else {
	// 		makeHtml(view::fetch());
	// 		//view::Display();
	// 	}
	// }
	private static $filename = P_PUBLIC . "index.html";

	public static function index()
	{
		//如要支持PC端和移动端自动切换模板，不建议再开启首页静态化自动生成，或使用首页静态缓存方法，只开启数据缓存即可
		if (siteconfig::getCache('indexhtml') == 1) {
			if (!is_file(self::$filename)) {
				$t = time() + siteconfig::getCache('indexhtmltime');
				$html = fopen(self::$filename, "w") or die("没有写入html权限！");
				fwrite($html, view::fetch() . '<script>var t= ' . $t . '; if(t < getServerDate()){ $.get("/ckhtml"); }</script>'); //前端根据JS获取服务端时间进行判断是否执行CKHTML方法
				setCache('indexMakehtmltime', time(), siteconfig::getCache('indexhtmltime')); //filemtime(self::$filename)
				fclose($html);
				view::Display();
			} else {
				die("网站已经生成html静态首页，请检查http代理是否将index.html设置成第一默认首页！！");
			}
		} else {
			view::Display();
		}
	}

	/*
	* 自动删除过期静态首页
	*/
	public static function ckhtml()
	{
		if (getCache('indexMakehtmltime') + siteconfig::getCache('indexhtmltime') <= TIME) {
			unlink(self::$filename);
		}
	}
}
