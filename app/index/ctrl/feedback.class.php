<?php
namespace ctrl;

use root\base\ctrl;
use model\feedbacks;
use z\view;

class feedback extends ctrl
{
	static function init(){
		\model\visits::insertData();//用户访问记录 写入记数
	}
	
    public static function index()
    {
		$list = feedbacks::selectData();
		view::assign('list',$list['data']);
		view::assign('page',$list['page']);
		view::display();
	}
	
	public static function info()
    {
 		if(!isset($_GET['id']) & empty($_GET['id']) & empty(ROUTE['params']['id'])){
			return  parent::_404();
		}
		
		$info = feedbacks::findData();
		if(empty($info)){
			return  parent::_404();
		}
		view::assign('info',$info);
		view::Display();
	}

	public static function addview()
    {
		view::Display();
	}

	public static function post(){
		$result = feedbacks::insertData();
  		if($result['status']){
			json(array('status'=> $result['status'],'info'=>$result['msg']));
		}else{
			json(array('status'=> $result['status'],'info'=>$result['msg']));
		}
	}
	
	/* 
	*  验证码
	*  $width;         //验证码图片的宽度
	*  $height;        //验证码图片的高度
	*  $codeNum;   //验证码字符的个数
	*  $fontSize;      //字符尺寸
	*/
	public static function vercode()
    {
        $ver = new \ext\verimg(100, 36, 4, 12);
        $ver->Img('cxuu_feedback');  ///将验证码的md5值存入$_SESSION['cxuu_feedback']中，以便后续验证使用；cxuu_feedback可以省略，默认为'_verimgcode'
	}
	
}
