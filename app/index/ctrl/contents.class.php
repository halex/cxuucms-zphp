<?php

namespace ctrl;

use root\base\ctrl;
use model\article;

use z\view;

class contents extends ctrl
{
	static function init()
	{
		\model\visits::insertData(); //用户访问记录 写入记数
	}

	public static function index()
	{
		if (empty($_GET['cid']) & empty(ROUTE['params']['cid'])) 
			return  parent::_404();

		$list = article::listData(ROUTE['query']['cid']);

		if (empty($list['data']))
			return  parent::_404();

		view::assign('list', $list['data']);
		view::assign('page', $list['page']);
		view::assign('cid', $list['cate']['id']);
		view::assign('urlname', $list['cate']['urlname']);
		view::assign('catename', $list['cate']['name']);
		if (empty($list['cate']['theme']))
			view::display();
		else
			view::display($list['cate']['theme']);
	}
}
