<?php
return [
    'PATH' => '', //此处是url的根路径，前后不带"/"，index可省略留空
    '/' => [
        'ctrl' => 'index',
        'act' => 'index',
    ],
    '/ckhtml' => [
        'ctrl' => 'index',
        'act' => 'ckhtml',
    ],
    '/info' => [
        'ctrl' => 'content',
        'act' => 'index',
		'params'=>['id']
    ],
	'/list' => [
        'ctrl' => 'contents',
        'act' => 'index',
		'params'=>['cid','p']
    ],
    '/visit' => [
        'ctrl' => 'visit',
        'act' => 'jsonList',
    ],
	'/image' => [
        'ctrl' => 'image',
        'act' => 'index',
		'params'=>['id']
    ],
	'/images' => [
        'ctrl' => 'imagelist',
        'act' => 'index',
		'params'=>['p']
    ],
	'/member' => [
        'ctrl' => 'member',
        'act' => 'index',
    ],
    '/feedbacks' => [
        'ctrl' => 'feedback',
        'act' => 'index',
        'params'=>['mid','p']
    ],
    '/feedback' => [
        'ctrl' => 'feedback',
        'act' => 'info',
        'params'=>['id']
    ],
    '/feedback/add' => [
        'ctrl' => 'feedback',
        'act' => 'addview'
    ],
    '/feedback/post' => [
        'ctrl' => 'feedback',
        'act' => 'post',
    ],
    '/feedback/vercode' => [
        'ctrl' => 'feedback',
        'act' => 'vercode',
    ],
    '*' => [ // 以上匹配不到时使用此路由
        'ctrl' => 'index',
        'act' => 'index',
    ],
];
