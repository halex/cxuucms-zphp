<?php
/* 
 *  视图列表循环
 *  文章内容使用方法：<?php foreach(cxuuList("table='article' cid='1,2,3' limit='10' cache='600' imgbl='0' attribute='0' order='id DESC'") as $vo){?> 
 */
function cxuuList($value)
{
	$data = stripslashes($value);
	preg_match_all('/([a-z0-9_]+)=[\'](.*)[\']/iU',$data,$matches);
	$arr = array_combine($matches[1],$matches[2]);
	return \model\common::get_List($arr);
}

/* 
 *  获取网站系统配置信息方法
 *  使用方法：在模板<?php echo siteInfo('copyright');?> 
 */
function siteInfo($key)
{
	return \model\siteconfig::getSiteInfo($key);
}

/* 
 *  路由 留言反馈内容页 URL 生成方法
 *  使用方法：在模板<?php echo urlFeedback($vo['id']);?> 
 */
function urlFeedback($val)
{
	switch ($GLOBALS['ZPHP_CONFIG']['ROUTER']['mod']) {
		case 2:
			$url = "/feedback/" . $val;
			break;
		case 0:
			$url = "/?c=feedback&a=info&id=" . $val;
			break;
		case 1:
			$url = "/feedback/info/id/" . $val;
			break;
	}
	return $url;
}
/* 
 *  路由 图集内容页 URL 生成方法
 *  使用方法：在模板<?php echo urlImage($vo['id']);?> 
 */
function urlImage($val)
{
	switch ($GLOBALS['ZPHP_CONFIG']['ROUTER']['mod']) {
		case 2:
			$url = "/image/" . $val;
			break;
		case 0:
			$url = "/?c=image&a=index&id=" . $val;
			break;
		case 1:
			$url = "/image/index/id/" . $val;
			break;
	}
	return $url;
}

/* 
 *  路由 内容页 URL 生成方法
 *  使用方法：在模板<?php echo urlInfo($vo['id']);?> 
 */
function urlInfo($val)
{
	switch ($GLOBALS['ZPHP_CONFIG']['ROUTER']['mod']) {
		case 2:
			$url = "/info/" . $val;
			break;
		case 0:
			$url = "/?c=content&a=index&id=" . $val;
			break;
		case 1:
			$url = "/content/index/id/" . $val;
			break;
	}
	return $url;
}


/* 
 *  路由 内容列表页 URL 生成方法
 *  使用方法：在模板<?php echo urlList($vo['id']);?> 
 */
function urlList($val)
{
	switch ($GLOBALS['ZPHP_CONFIG']['ROUTER']['mod']) {
		case 2:
			$url = "/list/" . $val;
			break;
		case 0:
			$url = "/?c=contents&a=index&cid=" . $val;
			break;
		case 1:
			$url = "/contents/index/cid/" . $val;
			break;
	}
	return $url;
}


/**
 * 截取字符
 * $title string
 * $end   number
 * 使用方法：在模板<?php echo cxuuMbStr($vo['title']);?> 
 * */
function cxuuMbStr($title, $end)
{
	if (mb_strlen($title, 'utf8') > $end -= 0) 
		$tit = mb_substr($title, 0, $end, 'utf8') . "..";
	 else 
		$tit = $title;
	return $tit;
}





/**
 * 栏目分页
 * $page,int $mod=0
 * 
 * $page 控制器分页数据变量，默认 $page
 * $mod  分页样式，0为原生样式，默认1 为LAYUI样式 设置为1时，需要配置LAYUI JS
 * 使用方法：在模板<?php echo cxuuPage($page,1);?> 
 * */
/* 原生原型
<div id="page">
		<div class="manu">
			<?php if($page['prev'] != "javascript:;"){?><a href="<{$page['first']}>">头页 </a><?php }?>
			<?php if($page['prev'] != "javascript:;"){?><span><a href="<{$page['prev']}>">&lt;</a></span><?php }?>
			<?php foreach($page['list'] as $key=>$vo){ ?>
			<?php if($vo != "javascript:;"){?><span class="disabled"><a href="<{$vo}>"><{$key}></a></span>
			<?php }else{ ?><span class="current"><a href="<?php echo $vo;?>"><?php echo $key;?></a></span><?php } ?>
			<?php }?>
			<?php if($page['next'] != "javascript:;"){?><span><a href="<{$page['next']}>">&gt; </a></span><?php }?>
			<?php if($page['last'] != "javascript:;"){?><span><a href="<{$page['last']}>">末页 </a></span><?php }?>
		</div>
	</div> */
function cxuuPage($page, $mod = 0)
{
	if ($mod == 1) {
		echo '<div id="laypage"></div>'; //layui样式
	} else {
		echo '<div id="page"><div class="manu">';
		if ($page['prev'] != "javascript:;")
			echo '<a href="' . $page['first'] . '">头页 </a>';
		if ($page['prev'] != "javascript:;")
			echo '<span><a href="' . $page['prev'] . '">&lt;</a></span>';
		foreach ($page['list'] as $key => $vo) {
			if ($vo != "javascript:;")
				echo '<span class="disabled"><a href="' . $vo . '">' . $key . '</a></span>';
			else
				echo '<span class="current"><a href="' . $vo . '">' . $key . '</a></span>';
		}
		if ($page['next'] != "javascript:;")
			echo '<span><a href="' . $page['next'] . '">&gt; </a></span>';

		if ($page['last'] != "javascript:;")
			echo '<span><a href="' . $page['last'] . '">末页 </a></span>';
		echo '</div></div>';
	}
}
