<?php
//要支持PC端和移动端自动切换模板，不建议再开启首页静态化自动生成，或使用首页静态缓存方法，只开启数据缓存即可
function is_mobile()
{
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $mobile_browser = array(
        "mqqbrowser", //手机QQ浏览器
        "opera mobi", //手机opera
        "juc", "iuc", //uc浏览器
        "fennec", "ios", "applewebKit/420", "applewebkit/525", "applewebkit/532", "ipad", "iphone", "ipaq", "ipod",
        "iemobile", "windows ce", //windows phone
        "240×320", "480×640", "acer", "android", "anywhereyougo.com", "asus", "audio", "blackberry", "blazer",
        "coolpad", "dopod", "etouch", "hitachi", "htc", "huawei", "jbrowser", "lenovo", "lg", "lg-",
        "lge-", "lge", "mobi", "moto", "nokia", "phone", "samsung", "sony", "symbian", "tablet", "tianyu", "wap", "xda", "xde", "zte",
        "MicroMessenger"
    );
    $is_mobile = false;
    foreach ($mobile_browser as $device) {
        if (stristr($user_agent, $device)) {
            $is_mobile = true;
            break;
        }
    }
    return $is_mobile;
}

if (is_mobile())
    return [
        'ROUTER' => [
            'mod' => 2,
        ],
        'VIEW' => [
            'theme' => 'mobile',
        ],
    ];
else
    return [
        'ROUTER' => [
            'mod' => 2,
        ],
        'VIEW' => [
            'theme' => 'default',
        ],
    ];
