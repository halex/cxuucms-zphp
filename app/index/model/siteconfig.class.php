<?php
namespace model;

class siteconfig
{
	//查找对应的网站设置信息
	// $val string  如： 系统设置：siteinfo  缓存设置：cache  上传设置：upload
	private static function getConfig(string $val)
	{
		$db = \ext\db::Init();
		$where['name'] = $val;
		$result = $db->table('siteconfig')->where($where)->cache(833600)->find();
		return unserialize($result['data']);
	}

	//根据数组键名查找对应的系统设置信息
    public static function getSiteInfo($key)
    {
		$result = self::getConfig('siteinfo');
        return $result[$key];
	}

	//根据数组键名查找对应的网站信息
    public static function getCache($key)
    {
		$result = self::getConfig('cache');
        return $result[$key];
	}

}
