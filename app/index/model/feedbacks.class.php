<?php
/**
* 在线意见提交数据库模型
* 龙啸轩内容管理系统 20200513
*/
namespace model;

use \common\extend\Check;

class feedbacks
{
    //查找一条数据
    public static function findData()
    {
        $db = \ext\db::Init();
        $where['id'] = ROUTE['query']['id'];
        $result = $db->table('feedbacks')->where($where)->find();
        return $result;
    }
	//查询列表
    public static function selectData()
    {
        $db = \ext\db::Init();
        $where['status'] = 1;
        $p = ROUTE['query']['p'];
        $num = 25;  //每页显示条数
        $page = ['num' => $num, 'p' => $p, 'return' => ['prev', 'next', 'first', 'last', 'list']];
		$result['data'] = $db->table('feedbacks')->where($where)->cache(600)->page($page)->order('id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
	
	//添加数据
	public static function insertData()
    {
        $checkVer = \ext\verimg::check($_POST['vercode'],'cxuu_feedback');    //cxuu_feedback 为默认值'_verimgcode'时可省略
        if(!$checkVer){
            return $result = ['status' => 0,'msg' => '验证码错误'];
        }

        $db = \ext\db::Init();   
        $data = [
			'title' => varFilter($_POST['title']),
			'name' => varFilter($_POST['name']),
			'phone' => varFilter($_POST['phone']),
            'content' => varFilter($_POST['content']),
            'mid' => $_POST['mid'],
			'ip' => getip(),
			'time' => date('Y-m-d H:i:s',time()),
            ];
        //验证数据
        $msg = Check::rule(
            array(check::len($data['title'],5,50),'标题为5-50个字符，请检查'),
            array(check::mobile($data['phone']),'手机号码不对，请检查'),
            );
        if($msg!==true){
            return $result = ['status' => 0,'msg' => $msg];
        }else{
            $re_key = $db->table('feedbacks')->insert($data);
            if($re_key){
                $result = ['key' => $re_key,'status' => 1,'msg' => '<b style="color:red">成功提交！</b><br>您现在还不能看到您写的内容！<br>您提交的信息将由系统后台审核后才能看到，请耐心等待！'];
            }else{
                $result = ['status' => 0,'msg' => $re_key];
            }
            return $result;
        }
    }
}
